﻿using System;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using movectl;
using movectlLib;
using Microsoft.Extensions.Logging;

namespace TestBLE
{
    class Program
    {
        static void Main3(string[] args)
        {
            LogManager.Factory.AddConsole();
            ILogger log = LogManager.Factory.CreateLogger("Main");
            //ulong btAddress = 227874729952872;
            ulong btAddress = 212622762443457;



            var spike = new GattSpike(btAddress);
            spike.Connect();
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            //new WindowsBLE();
            //Console.ReadLine();
            LogManager.Factory.AddConsole();
            ILogger log = LogManager.Factory.CreateLogger("Main");
            ulong btAddress = 243643032798336;


            //var moveNetworkDetector = new MoveNetworkDetector();
            //ulong? address = moveNetworkDetector.GetBtAddress(TimeSpan.FromSeconds(30));
            //if (!address.HasValue)
            //    throw new Exception("Move-meshnetwork not found");
            //btAddress = address.Value;


            var mesh = new CsrmeshNetworkUwp(1234, btAddress);
            mesh.Connect().GetAwaiter().GetResult();

            var controller = new MoveController(mesh);

            //controller.GetId(1);
            //Console.ReadLine();


            controller.SetPosition(0, 1);
            Console.ReadLine();

            controller.SetPosition(255, 1);
            Console.ReadLine();
            return;

            controller.SetPosition(255, 1);
            Console.ReadLine();
            controller.SetPosition(255, 2);
            log.Log(LogLevel.Information, $"set to 0");
            Console.ReadLine();

            ////controller.SetPosition(255, 1);
            ////log.Log(LogLevel.Information, $"set to 255");
            ////Console.ReadLine();

            //controller.SetPosition(50, 20, 1);
            //log.Log(LogLevel.Information, $"set to 0 with speed");
            //Console.ReadLine();

            //controller.SetPosition(100, 80, 1);
            //log.Log(LogLevel.Information, $"set to 0 with speed");
            //Console.ReadLine();

            //controller.SetPosition(150, 150, 1);
            //log.Log(LogLevel.Information, $"set to 0 with speed");
            //Console.ReadLine();

            //controller.SetPosition(200, 20, 1);
            //log.Log(LogLevel.Information, $"set to 0 with speed");
            //Console.ReadLine();

            //controller.SetPosition(150, 20, 1);
            //log.Log(LogLevel.Information, $"set to 0 with speed");
            //Console.ReadLine();

            //controller.SetPosition(100, 80, 1);
            //log.Log(LogLevel.Information, $"set to 0 with speed");
            //Console.ReadLine();

            //controller.SetPosition(50, 150, 1);
            //log.Log(LogLevel.Information, $"set to 0 with speed");
            //Console.ReadLine();

            //controller.SetPosition(0, 20, 1);
            //log.Log(LogLevel.Information, $"set to 0 with speed");
            //Console.ReadLine();

        }
    }
}
