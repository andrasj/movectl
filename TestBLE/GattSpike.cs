﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Foundation;
using Windows.Storage.Streams;
using movectlLib;
using Microsoft.Extensions.Logging;
using Move;
using Buffer = Windows.Storage.Streams.Buffer;

namespace TestBLE
{
    internal class GattSpike
    {
        private readonly ulong _btAddress;
        private readonly ILogger _logger;
        private BluetoothLEDevice _bluetoothLeDevice;

        public GattSpike(ulong btAddress)
        {
            _btAddress = btAddress;
            _logger = LogManager.Factory.CreateLogger("CsrUwp." + _btAddress);
        }

        public async void Connect()
        {
            //var bluetoothAdapter = await BluetoothAdapter.GetDefaultAsync();
            _bluetoothLeDevice = await BluetoothLEDevice.FromBluetoothAddressAsync(_btAddress);
            //  _bluetoothLeDevice = BluetoothLEDevice.FromBluetoothAddressAsync(bluetoothAdapter.BluetoothAddress, BluetoothAddressType.Random).GetAwaiter().GetResult();
            _logger.LogInformation("connected " + _bluetoothLeDevice);
            _bluetoothLeDevice.ConnectionStatusChanged += (sender, args) =>
                _logger.Log(LogLevel.Information, $"ConnectedChanged: {_bluetoothLeDevice.ConnectionStatus}");

            //   _bluetoothLeDevice.get
            var gattDeviceServicesResult = await _bluetoothLeDevice.GetGattServicesAsync();
            if (gattDeviceServicesResult.Status != GattCommunicationStatus.Success)
                _logger.LogError("could not get services");
            foreach (GattDeviceService gsvc in gattDeviceServicesResult.Services)
            {
                var chars = await gsvc.GetCharacteristicsAsync();
                if (chars.Status != GattCommunicationStatus.Success)
                    _logger.LogError("could not get characteristics");
                foreach (GattCharacteristic c in chars.Characteristics)
                {
                    //var gattDescriptorsResult = await c.GetDescriptorsAsync();
                    //if (gattDescriptorsResult.Status != GattCommunicationStatus.Success)
                    //    _logger.LogError("could not get descriptors");
                    //foreach (var gattDescriptor in gattDescriptorsResult.Descriptors)
                    {
                        _logger.LogInformation($"svc: {gsvc.Uuid} / 0x{gsvc.AttributeHandle:X} / {gsvc.SharingMode}   " + Environment.NewLine
                                               + $"charac: {c.Uuid} / 0x{c.AttributeHandle:X} / {c.UserDescription} / {c.ProtectionLevel} / {c.CharacteristicProperties} / {c.PresentationFormats.Count}" + Environment.NewLine
                                             //+ $"desc: {gattDescriptor.Uuid} / 0x{gattDescriptor.AttributeHandle:x} / {gattDescriptor.ProtectionLevel}"
                                               );

                    }
                }
            }
            _logger.LogInformation("iteration done");
        }

        public void SendPacket(byte[] packetData, GattCharacteristic charac)
        {
            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;
            writer.WriteBytes(packetData);
            var result = charac.WriteValueAsync(writer.DetachBuffer()).GetAwaiter().GetResult();
            _logger.Log(result == GattCommunicationStatus.Success ? LogLevel.Information : LogLevel.Error, "Send finished: " + result);
        }
        //public override void SendPacket(byte[] packetData, ushort baseHandle)
        //{
        //    var gattServices = _bluetoothLeDevice.GetGattServicesAsync().GetAwaiter().GetResult();
        //    if (gattServices.Status != GattCommunicationStatus.Success)
        //        throw new Exception("operation services error: " + gattServices.ProtocolError);

        //    foreach (GattDeviceService service in gattServices.Services)
        //    {
        //        _logger.Log(LogLevel.Information, $"Gatt Service: {service.Uuid}");
        //        GattCharacteristicsResult characteristics = service.GetCharacteristicsAsync(BluetoothCacheMode.Uncached).GetAwaiter().GetResult();
        //        if (characteristics.Status != GattCommunicationStatus.Success)
        //            throw new Exception("operation characteristics error: " + characteristics.ProtocolError);
        //        foreach (GattCharacteristic characteristic in characteristics.Characteristics)
        //        {
        //            _logger.Log(LogLevel.Information, $"  Gatt characteristic: {characteristic.Uuid}, hndl: {characteristic.AttributeHandle}, {characteristic.UserDescription}");
        //            if (characteristic.AttributeHandle < 28 | characteristic.AttributeHandle > 36)
        //            {
        //                _logger.LogInformation("Skipping");
        //                continue;
        //            }

        //            try
        //            {
        //                var writer = new DataWriter();
        //                writer.ByteOrder = ByteOrder.LittleEndian;
        //                writer.WriteBytes(packetData);
        //                var result = characteristic.WriteValueAsync(writer.DetachBuffer()).GetAwaiter().GetResult();
        //                _logger.Log(result == GattCommunicationStatus.Success ? LogLevel.Information : LogLevel.Error, "Send finished: " + result);

        //                _logger.Log(LogLevel.Warning, "OK?");

        //            }
        //            catch (Exception e)
        //            {
        //                _logger.LogError(e, "KABOOM");
        //            }
        //        }
        //    }
        //    _logger.Log(LogLevel.Information, $"Press enter to quit");
        //}
    }
}