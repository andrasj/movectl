﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Foundation;
using Windows.Storage.Streams;
using movectlLib;
using Microsoft.Extensions.Logging;
using Move;

namespace TestBLE
{
    internal class CsrmeshNetworkUwp : CsrmeshNetworkBase
    {
        private readonly ulong _btAddress;
        private readonly ILogger _logger;
        private BluetoothLEDevice _bluetoothLeDevice;
        private GattCharacteristic _gattCharacteristic;
        private GattCharacteristic _gattCharacteristic2;
        private GattDeviceServicesResult _gattServicesForUuidAsync;
        private GattDeviceService _gattDeviceService;

        public CsrmeshNetworkUwp(int passKey, ulong btAddress) : base(passKey)
        {
            _btAddress = btAddress;
            _logger = LogManager.Factory.CreateLogger($"CsrUwp.{_btAddress:X}");
        }

        public async Task<IAsyncOperation<GattDeviceServicesResult>> GetGattServicesAsync()
        {
            _logger.Log(LogLevel.Information, $"found device: {_bluetoothLeDevice.Name}");
            var gattDeviceServicesResult = await _bluetoothLeDevice.GetGattServicesAsync((BluetoothCacheMode.Uncached));
            //   gattDeviceServicesResult.Services.First().GetAllCharacteristics().Single(c => c.AttributeHandle == 0x21);
            return _bluetoothLeDevice.GetGattServicesAsync();
            //   return fromBluetoothAddressAsync.GattServices;

        }

        public async Task Connect()
        {

            _bluetoothLeDevice = await BluetoothLEDevice.FromBluetoothAddressAsync(_btAddress);
            _bluetoothLeDevice.ConnectionStatusChanged += (sender, args) =>
                _logger.Log(LogLevel.Information, $"ConnectedChanged: {_bluetoothLeDevice.ConnectionStatus}");
            _gattServicesForUuidAsync = await _bluetoothLeDevice.GetGattServicesForUuidAsync(Guid.Parse("0000fef1-0000-1000-8000-00805f9b34fb"), BluetoothCacheMode.Uncached);
            if (_gattServicesForUuidAsync.Status != GattCommunicationStatus.Success)
                _logger.LogError("could not get services");
            _gattDeviceService = _gattServicesForUuidAsync.Services.Single();
            GattCharacteristicsResult gattCharacteristicsResult = await _gattDeviceService.GetCharacteristicsForUuidAsync(Guid.Parse("c4edc000-9daf-11e3-8004-00025b000b00"), BluetoothCacheMode.Uncached);
            if (gattCharacteristicsResult.Status != GattCommunicationStatus.Success)
                _logger.LogError("could not get characteristic");
            _gattCharacteristic = gattCharacteristicsResult.Characteristics.Single();
            _logger.LogInformation($"handle for first 20 bytes: 0x{_gattCharacteristic.AttributeHandle:X4}");
            GattCharacteristicsResult gattCharacteristicsResult2 = await _gattDeviceService.GetCharacteristicsForUuidAsync(Guid.Parse("c4edc000-9daf-11e3-8003-00025b000b00"), BluetoothCacheMode.Uncached);
            if (gattCharacteristicsResult2.Status != GattCommunicationStatus.Success)
                _logger.LogError("could not get characteristic");
            _gattCharacteristic2 = gattCharacteristicsResult2.Characteristics.Single();
            _logger.LogInformation($"handle for >20 bytes: 0x{_gattCharacteristic2.AttributeHandle:X4}");
            _gattCharacteristic.ValueChanged += OnGattCharacteristicOnValueChanged;
            _gattCharacteristic2.ValueChanged += OnGattCharacteristicOnValueChanged;
            var subsResult = await _gattCharacteristic.WriteClientCharacteristicConfigurationDescriptorWithResultAsync(GattClientCharacteristicConfigurationDescriptorValue.Notify);
            var subs2Result = await _gattCharacteristic2.WriteClientCharacteristicConfigurationDescriptorWithResultAsync(GattClientCharacteristicConfigurationDescriptorValue.Notify);
        }

        private void OnGattCharacteristicOnValueChanged(GattCharacteristic sender, GattValueChangedEventArgs args)
        {
            var buffer = args.CharacteristicValue;
            int length = Math.Min((int)buffer.Length, 20);
            if (length == 0)
            {
                _logger.LogInformation(
                    $"charac 0x{sender.AttributeHandle:X2} No Data");
                return;
            }
            byte[] cpy = buffer.ToArray(0, length);
            _logger.LogInformation(
                $"charac 0x{sender.AttributeHandle:X2} Value ({buffer.Length} bytes): 0x{string.Concat(cpy.Select(b => b.ToString("X2")))}");
        }

        public async Task SendPacketAsync(byte[] packetData)
        {
            var encryptedPacket = MakePacket(packetData);

            var dataPackets = new List<Tuple<byte[], GattCharacteristic>>();

            if (encryptedPacket.Length > 20)
            {
                dataPackets.Add(Tuple.Create(encryptedPacket.Skip(20).ToArray(), _gattCharacteristic2));
                dataPackets.Add(Tuple.Create(encryptedPacket.Take(20).ToArray(), _gattCharacteristic));
            }
            else
            {
                dataPackets.Add(Tuple.Create(encryptedPacket, _gattCharacteristic));
            }

            _logger.LogInformation($"Writing data: {packetData.Length:D5} bytes: 0x{string.Concat(packetData.Select(b => b.ToString("X2")))}" + Environment.NewLine
                                   + $"encrypted:   {encryptedPacket.Length:D5} bytes: 0x{string.Concat(encryptedPacket.Select(b => b.ToString("X2")))}");

            foreach (var packetToSend in dataPackets)
            {
                var csrData = packetToSend.Item1;
                var charactiristic = packetToSend.Item2;
                _logger.LogInformation($"data to network: 0x{string.Concat(csrData.Select(b => b.ToString("X2")))} to 0x{charactiristic.AttributeHandle:X2}");
                using (var writer = new DataWriter())
                {
                    writer.WriteBytes(csrData);
                    var detachBuffer = writer.DetachBuffer();
                    var result = await charactiristic.WriteValueWithResultAsync(detachBuffer, GattWriteOption.WriteWithResponse);
                    _logger.Log(result.Status == GattCommunicationStatus.Success ? LogLevel.Information : LogLevel.Error, "Send finished: " + result);
                }

            }

            //var readResult = await _gattCharacteristic.ReadValueAsync(BluetoothCacheMode.Uncached);
            //_logger.Log(readResult.Status == GattCommunicationStatus.Success ? LogLevel.Information : LogLevel.Error, $"read finished: {readResult.Status}");
            //if (readResult.Status == GattCommunicationStatus.Success)
            //    _logger.Log(LogLevel.Information, $"value: 0x{string.Concat(readResult.Value.ToArray().Select(b => b.ToString("X2")))}");
            //if (readResult.Status == GattCommunicationStatus.ProtocolError)
            //{
            //    if (readResult.ProtocolError.Value == GattProtocolError.AttributeNotFound) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.AttributeNotLong) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.InsufficientAuthentication) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.InsufficientAuthorization) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.InsufficientEncryption) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.InsufficientEncryptionKeySize) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.InsufficientResources) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.InvalidAttributeValueLength) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.InvalidHandle) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.InvalidOffset) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.InvalidPdu) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.PrepareQueueFull) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.ReadNotPermitted) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.RequestNotSupported) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.UnlikelyError) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.UnsupportedGroupType) { }
            //    if (readResult.ProtocolError.Value == GattProtocolError.WriteNotPermitted) { }

            //}
        }
        //public override void SendPacket(byte[] packetData, ushort baseHandle)
        //{
        //    var gattServices = _bluetoothLeDevice.GetGattServicesAsync().GetAwaiter().GetResult();
        //    if (gattServices.Status != GattCommunicationStatus.Success)
        //        throw new Exception("operation services error: " + gattServices.ProtocolError);

        //    foreach (GattDeviceService service in gattServices.Services)
        //    {
        //        _logger.Log(LogLevel.Information, $"Gatt Service: {service.Uuid}");
        //        GattCharacteristicsResult characteristics = service.GetCharacteristicsAsync(BluetoothCacheMode.Uncached).GetAwaiter().GetResult();
        //        if (characteristics.Status != GattCommunicationStatus.Success)
        //            throw new Exception("operation characteristics error: " + characteristics.ProtocolError);
        //        foreach (GattCharacteristic characteristic in characteristics.Characteristics)
        //        {
        //            _logger.Log(LogLevel.Information, $"  Gatt characteristic: {characteristic.Uuid}, hndl: {characteristic.AttributeHandle}, {characteristic.UserDescription}");
        //            if (characteristic.AttributeHandle < 28 | characteristic.AttributeHandle > 36)
        //            {
        //                _logger.LogInformation("Skipping");
        //                continue;
        //            }

        //            try
        //            {
        //                var writer = new DataWriter();
        //                writer.ByteOrder = ByteOrder.LittleEndian;
        //                writer.WriteBytes(packetData);
        //                var result = characteristic.WriteValueAsync(writer.DetachBuffer()).GetAwaiter().GetResult();
        //                _logger.Log(result == GattCommunicationStatus.Success ? LogLevel.Information : LogLevel.Error, "Send finished: " + result);

        //                _logger.Log(LogLevel.Warning, "OK?");

        //            }
        //            catch (Exception e)
        //            {
        //                _logger.LogError(e, "KABOOM");
        //            }
        //        
        //    }
        //    _logger.Log(LogLevel.Information, $"Press enter to quit");
        //}
        public override void SendPacket(byte[] packetData, ushort baseHandle)
        {
            if (baseHandle != 0x21)
                throw new NotSupportedException();
            SendPacketAsync(packetData).GetAwaiter().GetResult();

            //ReadHandle("c4edc000-9daf-11e3-8001-00025b000b00");
            //ReadHandle("c4edc000-9daf-11e3-8002-00025b000b00");
            //ReadHandle("c4edc000-9daf-11e3-8005-00025b000b00");
            //ReadHandle("c4edc000-9daf-11e3-8006-00025b000b00");
        }

        private void ReadHandle(string uuid)
        {
            var characteristicsResult = _gattDeviceService
                .GetCharacteristicsForUuidAsync(Guid.Parse(uuid), BluetoothCacheMode.Uncached).GetAwaiter().GetResult();
            if (characteristicsResult.Status != GattCommunicationStatus.Success)
                _logger.LogError(
                    $"characteristicsResult {characteristicsResult.Status} / {characteristicsResult.ProtocolError}");
            var charac = characteristicsResult.Characteristics.Single();
            var readResult = charac.ReadValueAsync(BluetoothCacheMode.Uncached).GetAwaiter().GetResult();
            if (readResult.Status != GattCommunicationStatus.Success)
                _logger.LogError(
                    $"charac 0x{charac.AttributeHandle:X2} readResult {readResult.Status} / {readResult.ProtocolError}");
            else
            {
                var buffer = readResult.Value;
                int length = Math.Min((int)buffer.Length, 20);
                if (length == 0)
                {
                    _logger.LogInformation(
                        $"charac 0x{charac.AttributeHandle:X2} No Data");
                    return;
                }
                byte[] cpy = buffer.ToArray(0, length);
                _logger.LogInformation(
                    $"charac 0x{charac.AttributeHandle:X2} Value ({buffer.Length} bytes): 0x{string.Concat(cpy.Select(b => b.ToString("X2")))}");
            }
        }
    }
}