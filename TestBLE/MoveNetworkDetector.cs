﻿using System;
using System.Threading;
using Windows.Devices.Bluetooth.Advertisement;
using movectlLib;
using Microsoft.Extensions.Logging;

namespace TestBLE
{
    public class MoveNetworkDetector
    {
        private ILogger _logger = LogManager.Factory.CreateLogger("MoveNetworkDetector");

        public ulong? GetBtAddress(TimeSpan timeout)
        {
            ManualResetEvent syncEvent = new ManualResetEvent(false);
            ulong? btAddress = null;

            var watcher = new BluetoothLEAdvertisementWatcher();

            watcher.ScanningMode = BluetoothLEScanningMode.Active;

            // Only activate the watcher when we're recieving values >= -80
            watcher.SignalStrengthFilter.InRangeThresholdInDBm = -80;

            // Stop watching if the value drops below -90 (user walked away)
            watcher.SignalStrengthFilter.OutOfRangeThresholdInDBm = -90;

            // Register callback for when we see an advertisements
            watcher.Received += (sender, args) =>
            {
                _logger.LogInformation($"received advertisement from {args.AdvertisementType}: {args.Advertisement.LocalName}");
                btAddress = args.BluetoothAddress;
                syncEvent.Set();
            };

            // Wait 5 seconds to make sure the device is really out of range
            watcher.SignalStrengthFilter.OutOfRangeTimeout = TimeSpan.FromMilliseconds(5000);
            watcher.SignalStrengthFilter.SamplingInterval = TimeSpan.FromMilliseconds(1000);

            watcher.AdvertisementFilter.Advertisement.LocalName = "MOVE";

            // Starting watching for advertisements
            _logger.LogInformation($"start scanning for devices");
            watcher.Start();

            syncEvent.WaitOne(timeout);

            watcher.Stop();

            return btAddress;
        }
    }
}
