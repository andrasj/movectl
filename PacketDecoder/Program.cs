﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Move;
using movectl;
using Newtonsoft.Json.Linq;

namespace PacketDecoder
{
    class PacketDecryptor : CsrmeshNetworkBase
    {
        public PacketDecryptor(int pin) : base(pin, null)
        {
        }

        protected override Task DoConnect()
        {
            throw new NotImplementedException();
        }

        protected override Task DoSendPacketAsync(byte[] packetData)
        {
            throw new NotImplementedException();
        }

        protected override Task DoSubscribeNotifications()
        {
            throw new NotImplementedException();
        }
    }
    
    public class Program
    {
        private static PacketDecryptor decryptor;

        public static void Main(string[] args)
        {
            movectlLib.LogManager.Factory = LoggerFactory.Create(builder=>builder.AddConsole());
            bool done = false;
            int pin=4242;
            decryptor = new PacketDecryptor(pin);

            do
            {
                Console.WriteLine("input (q, c <hex>, n <hex>, f <filename>): ");
                var input = Console.ReadLine().Trim();
                input = input.Trim();
                var delimPos = input.IndexOf(' ');
                string cmd = delimPos<0 ? input : input.Substring(0,delimPos);
                string cmdArgs = delimPos<0 ? "" : input.Substring(delimPos+1);

                switch (cmd){
                    case "q":
                        done = true;
                        break;
                    case "c":
                        DecryptCommand(cmdArgs);
                        break;
                    case "n":
                        DecryptNotification(cmdArgs);
                        break;
                    case "f":
                        DecryptCaptureFile(cmdArgs,true);
                        break;
                    case "rf":
                        DecryptCaptureFile(cmdArgs,false);
                        break;
                    default:
                        Console.WriteLine("ERROR: unknown command");
                        break;
                }
            }while (!done);
        }

        private static void DecryptCaptureFile(string filename,bool excludeWakeFrames)
        {
//            filename = "/home/andrasj/Downloads/logscenario2b/FS/data/misc/bluetooth/logs/scenario2_ATT.json";

            string jsonString = System.IO.File.ReadAllText(filename);
            var jPackets = JArray.Parse(jsonString);

            var meshFrames = GetMeshFrames(jPackets);

            foreach (var frame in meshFrames)
            {
                switch (frame.Direction){
                    case MeshFrame.DirectionType.FromNetwork:
                        {
                            byte[] decryptedNotificationPacket = decryptor.DecryptResponsePacket(frame.PacketData);
                            try {
                                var resp = MoveResp.Parse(decryptedNotificationPacket);
                                Console.WriteLine($"{frame.Timestamp.ToLocalTime():HH:mm:ss,fff} | {resp} | RCV: {ToHex(frame.PacketData)} | DEC: {ToHex(decryptedNotificationPacket)}");
                            }
                            catch (Exception e){
                                Console.WriteLine($"{frame.Timestamp.ToLocalTime():HH:mm:ss,fff} | ERR: {e.Message} | RCV: {ToHex(frame.PacketData)} | DEC: {ToHex(decryptedNotificationPacket)}");
                            }
                        }
                        break;
                    case MeshFrame.DirectionType.ToNetwork:
                        {
                            byte[] decryptedCommandPacket = decryptor.DecryptPacket(frame.PacketData);
                            try {
                                var cmd = MoveCmd.Parse(decryptedCommandPacket);
                                if (excludeWakeFrames && cmd is WakeMesh)
                                    continue;
                                Console.WriteLine($"{frame.Timestamp.ToLocalTime():HH:mm:ss,fff} | {cmd} | SND: {ToHex(frame.PacketData)} | DEC: {ToHex(decryptedCommandPacket)}");
                            }
                            catch (Exception e){
                                Console.WriteLine($"{frame.Timestamp.ToLocalTime():HH:mm:ss,fff} | ERR: {e.Message} | SND: {ToHex(frame.PacketData)} | DEC: {ToHex(decryptedCommandPacket)}");
                            }
                        }
                        break;
                    default:
                        throw new ArgumentException("Direction");
                }
            }

        
            // var packets = ExtractMeshFrames()
        }

        public static DateTime FromUnixTime(double unixTime)
        {
            return epoch.AddSeconds(unixTime);
        }
        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private static IEnumerable<MeshFrame> GetMeshFrames(JArray jPackets)
        {
            string pendingRCV=null;
            string pendingSND=null;

            foreach (JObject jPacket in jPackets)
            {
                double epoch = jPacket["_source"]["layers"]["frame"].Value<double>("frame.time_epoch");
                DateTime tms = FromUnixTime(epoch);
                int battOpcode = int.Parse(jPacket["_source"]["layers"]["btatt"].Value<string>("btatt.opcode").Replace("0x",""),NumberStyles.HexNumber|NumberStyles.AllowHexSpecifier);
                int battHandle = int.Parse(jPacket["_source"]["layers"]["btatt"].Value<string>("btatt.handle").Replace("0x",""),NumberStyles.HexNumber|NumberStyles.AllowHexSpecifier);
                string battValueHex = jPacket["_source"]["layers"]["btatt"].Value<string>("btatt.value");

                if (battHandle != 0x21 && battHandle!= 0x1e)
                    throw new Exception("Invalid handle");
                bool isLastFragment = battHandle == 0x21;
                MeshFrame.DirectionType dir = battOpcode == 0x1b ? MeshFrame.DirectionType.FromNetwork : battOpcode == 0x52 ? MeshFrame.DirectionType.ToNetwork : throw new Exception("unknow opcode");

                switch (dir) {
                    case MeshFrame.DirectionType.FromNetwork:
                    {
                        if (isLastFragment) {
                            byte[] allData = ToBin((pendingRCV ?? "") + battValueHex);
                            pendingRCV = null;
                            yield return new MeshFrame(dir,allData,tms);
                        } else {
                            if (pendingRCV != null)
                                throw new Exception("multiple incomplete RCV packets... not expected or implemented yet, need to find out more");

                            pendingRCV = battValueHex;
                        }
                    }
                    break;
                    case MeshFrame.DirectionType.ToNetwork:
                    {
                        if (isLastFragment) {
                            byte[] allData = ToBin((pendingSND ?? "") + battValueHex);
                            pendingSND = null;
                            yield return new MeshFrame(dir,allData,tms);
                        } else {
                            if (pendingSND != null)
                                throw new Exception("multiple incomplete SND packets... not expected or implemented yet, need to find out more");

                            pendingSND = battValueHex;
                        }
                    }
                    break;
                    default:
                        throw new ArgumentException("framedirection");
                }
            }

        }

        private static void DecryptNotification(string cmdArgs)
        {
                var input = cmdArgs.Replace(":", "");
                byte[] encrtypedBytes = ToBin(input);

                 byte[] data = decryptor.DecryptResponsePacket(encrtypedBytes);
                 var resp = MoveResp.Parse(data);
                 Console.WriteLine($"decrypted response data: 0x{ToHex(data)} | {resp}");
        }

        private static void DecryptCommand(string cmdArgs)
        {
                var input = cmdArgs.Replace(":", "");
                byte[] encrtypedBytes = ToBin(input);

                byte[] data = decryptor.DecryptPacket(encrtypedBytes);
                var cmd = MoveCmd.Parse(data);
                Console.WriteLine($"decrypted request data: 0x{ToHex(data)} | {cmd}");
        }

        private static string ToHex(byte[] data)=>BitConverter.ToString(data).Replace("-",":");

        private static byte[] ToBin(string hexData)
        {
            hexData=hexData.Replace(":","");
            if (hexData.Length % 2 == 1)
                throw new Exception("invalid data, should have even number of hex-chars");

            byte[] result = new byte[hexData.Length / 2];
            for (int byteNb = 0; byteNb < result.Length; byteNb++)
            {
                result[byteNb] = byte.Parse(hexData.Substring(byteNb * 2, 2), NumberStyles.HexNumber);
            }

            return result;
        }
    }

    internal class MeshFrame
    {
        public enum DirectionType {
            FromNetwork,
            ToNetwork
        }

        public MeshFrame(DirectionType direction, byte[] packetData, DateTime timestamp)
        {
            Direction = direction;
            PacketData = packetData;
            Timestamp = timestamp;
        }

        public DirectionType Direction {get;}
        public byte[] PacketData {get;}
        public DateTime Timestamp {get;}
    }
}
