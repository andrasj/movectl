using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HashtagChris.DotNetBlueZ;
using HashtagChris.DotNetBlueZ.Extensions;
using movectlLib;

namespace testBleLinux{
  public class MoveDetectorDBus
    {
        private ILogger _logger = LogManager.Factory.CreateLogger("MoveNetworkDetector");

        public string GetBtAddress(TimeSpan timeout){
            return GetBtAddress_internal(timeout).GetAwaiter().GetResult();
        }

        private async Task<string> GetBtAddress_internal(TimeSpan timeout)
        {
            ManualResetEvent syncEvent = new ManualResetEvent(false);
            string btAddress = null;

           var adapters = await BlueZManager.GetAdaptersAsync();
            if (adapters.Count == 0)
            {
              throw new Exception("No Bluetooth adapters found.");
            }
            var adapter = adapters.First();
            adapter.DeviceFound += async (sender, args) =>
            {
                using var device = args.Device;
                //var deviceDescription = await GetDeviceDescriptionAsync(device);
                var deviceAddress = await device.GetAddressAsync();
                var deviceName = await device.GetAliasAsync();
                _logger.LogInformation($"received advertisement from {deviceAddress}: {deviceName}");
                if (deviceAddress.Equals("MOVE", StringComparison.OrdinalIgnoreCase) || deviceAddress.Equals("_CSRmesh", StringComparison.OrdinalIgnoreCase)
                   || deviceName.Contains("MOVE", StringComparison.OrdinalIgnoreCase)|| deviceName.Contains("_CSRmesh", StringComparison.OrdinalIgnoreCase))
                {
                    btAddress = deviceAddress;
                    syncEvent.Set();
                }
            };

            // Starting watching for advertisements
            _logger.LogInformation($"start scanning for devices");
            await adapter.StartDiscoveryAsync();

            syncEvent.WaitOne(timeout);

            await adapter.StopDiscoveryAsync();

            adapters.ToList().ForEach(a=>a.Dispose());

            return btAddress;
        }
    }
}