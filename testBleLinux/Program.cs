﻿using movectl;
using movectlLib;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HashtagChris.DotNetBlueZ;
using HashtagChris.DotNetBlueZ.Extensions;
using Microsoft.Extensions.Logging;
using LoggingAdvanced.Console;

namespace testBleLinux
{
    class Program
{
  static async Task Main(string[] args)
  {

    LogManager.Factory = LoggerFactory.Create(builder=>
                        builder.AddFilter("Csr",LogLevel.Debug)
                        .AddFilter("Csr.Packet",LogLevel.Debug)
                        .AddFilter("MoveController",LogLevel.Information)
                        .AddConsole(opts => opts.TimestampFormat = "HH:mm:ss,fff: ")
                        );

    ILogger log = LogManager.Factory.CreateLogger("Main");
//    string btAddress = "D5:39:DC:38:82:F0";
    string btAddress = "43:C5:5B:04:00:06";


// log.LogInformation("Discovering Mesh");
//     var moveNetworkDetector = new testBleLinux.MoveDetectorDBus();
//     btAddress = moveNetworkDetector.GetBtAddress(TimeSpan.FromSeconds(30));
//     if (btAddress == null)
//        throw new Exception("Move-meshnetwork not found");


    using var mesh = new CsrmeshNetworkDBus(4242, btAddress);
log.LogInformation("Connecting Mesh");
    await mesh.Connect();

    var controller = new MoveController(mesh);
log.LogInformation("Waking mesh");
for(int i=0;i<3;i++)
  {
    await Task.Delay(357);
    controller.WakeMesh();
  }

  await Task.Delay(500);
  log.LogInformation("Performing Actions");

for(int i=0;i<5;i++)
    {

//       var result1 = controller.GetStatus(1);
//       log.LogInformation($"Dev1: @{result1.Position}-{result1.Battery}");
//  await Task.Delay(400);
//       var result2 = controller.GetStatus(2);
//       log.LogInformation($"Dev2: @{result2.Position}-{result2.Battery}");

//  await Task.Delay(4000);
      const int speed = 0x4F;
      controller.SetPosition(0x42, speed, speed, 2);
//  await Task.Delay(400);
//      controller.SetPosition(0xF0, speed, speed, 1);

 await Task.Delay(5000);

    controller.SetPosition(0x84, speed, speed, 2);
//  await Task.Delay(400);
//      controller.SetPosition(0x0F, speed, speed, 1);
 await Task.Delay(5000);

    }
return;


}
}
}
