﻿using HashtagChris.DotNetBlueZ;
using HashtagChris.DotNetBlueZ.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using movectlLib;
using movectl;
using Microsoft.Extensions.Logging;
using Move;

namespace testBleLinux
{
    internal class CsrmeshNetworkDBus : CsrmeshNetworkBase, IDisposable
    {
        private readonly string _btAddress;
        private readonly ILogger _logger;

        private Adapter _adapter;
        private Device _device;
        private IGattService1 _service;
        public HashtagChris.DotNetBlueZ.GattCharacteristic _characteristic;
        public HashtagChris.DotNetBlueZ.GattCharacteristic _characteristic2;

        public CsrmeshNetworkDBus(int passKey, string btAddress) : base(passKey)
        {
            _btAddress = btAddress;
            _logger = LogManager.Factory.CreateLogger($"Csr.DBUS.{_btAddress:X}");
        }

        public void Dispose(){
            _characteristic2?.Dispose();
            _characteristic?.Dispose();
            _device.DisconnectAsync().GetAwaiter().GetResult();
            _device?.Dispose();
            _adapter?.Dispose();
        }


        private async void InitBleAdapter()
        {
            var adapters = await BlueZManager.GetAdaptersAsync();
            if (adapters.Count == 0)
            {
                throw new Exception("No Bluetooth adapters found.");
            }

            _adapter = adapters.First();

            var adapterPath = _adapter.ObjectPath.ToString();
            var adapterName = adapterPath.Substring(adapterPath.LastIndexOf("/") + 1);
            var adapterAddress = (await _adapter.GetAllAsync()).Address;
            _logger.Log(LogLevel.Information,$"Using Bluetooth adapter {adapterName} ({adapterAddress})");

        }

        protected override async Task DoConnect()
        {
            InitBleAdapter();

            _device = await _adapter.GetDeviceAsync(_btAddress);
            if (_device == null)
            {
            throw new Exception($"Bluetooth peripheral with address '{_btAddress}' not found. Use `bluetoothctl` or Bluetooth Manager to scan and possibly pair first.");
            }

            _logger.LogInformation($"Connecting to {_btAddress}");
            await _device.ConnectAsync();
            await Task.Delay(200);
            _logger.LogInformation($"Retrieving service");
            _service = await _device.GetServiceAsync("0000fef1-0000-1000-8000-00805f9b34fb");
            await Task.Delay(200);
            _logger.LogInformation($"Retrieving characteristics");
            _characteristic = await _service.GetCharacteristicAsync("c4edc000-9daf-11e3-8004-00025b000b00");
            await Task.Delay(200);
            _characteristic2 = await _service.GetCharacteristicAsync("c4edc000-9daf-11e3-8003-00025b000b00");
            await Task.Delay(200);
        }

         protected override async Task DoSubscribeNotifications()
        {
            _logger.LogInformation($"Registering characteristics notifications");
            _characteristic.Value += characteristic_ValueNotification;
            await _characteristic.StartNotifyAsync();
            _characteristic2.Value += characteristic_ValueNotification;
            await _characteristic2.StartNotifyAsync();
            _logger.LogInformation($"Connected to mesh network");
            await Task.Delay(200);
        }

        private int GetHandle(GattCharacteristic characteristic){
            if (characteristic == _characteristic)
                return 0x21;
            else if (characteristic == _characteristic2)
                return 0x1e;

            throw new ArgumentException("unknown characteristic");
        }

        private byte[] pendingData = null;

        private async Task characteristic_ValueNotification(GattCharacteristic characteristic, GattCharacteristicValueEventArgs e)
        {
            var handle = GetHandle(characteristic);

            _logger.LogInformation($"Characteristic '{handle}' value (hex): {BitConverter.ToString(e.Value).Replace("-",":")}");

            if (characteristic == _characteristic)
            {//complete
                byte[] data =(pendingData ?? Enumerable.Empty<byte>()).Concat(e.Value).ToArray();
                base.ProcessNotificationData(data);
            }
            else{//expect some more
                pendingData = e.Value;
            }

            await Task.Delay(0);
        }


        protected override async Task DoSendPacketAsync(byte[] packetData)
        {
            var dataPackets = new List<Tuple<byte[], GattCharacteristic>>();

            if (packetData.Length > 20)
            {
                dataPackets.Add(Tuple.Create(packetData.Take(20).ToArray(), _characteristic2));
                dataPackets.Add(Tuple.Create(packetData.Skip(20).ToArray(), _characteristic));
            }
            else
            {
                dataPackets.Add(Tuple.Create(packetData, _characteristic));
            }


            foreach (var packetToSend in dataPackets)
            {
                var data = packetToSend.Item1;
                var characteristic = packetToSend.Item2;
                _logger.LogDebug($"data to network: 0x{string.Join(":",data.Select(b => b.ToString("X2")))} to 0x{GetHandle(characteristic):X2}");
                await characteristic.WriteValueAsync(data, new Dictionary<string,object>{{"type","command"}});
            }
            _logger.LogDebug($"data sent");

        }
    }
}