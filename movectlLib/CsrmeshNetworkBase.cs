using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using movectl;
using movectlLib;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Move
{
    public abstract class CsrmeshNetworkBase
    {
        private const int MaxSequenceNr = 16777215;
        protected string _dst;

        protected static readonly ILogger _log = LogManager.Factory.CreateLogger("Csr");
        protected static readonly ILogger _packetLog = LogManager.Factory.CreateLogger("Csr.Packets");
        private int SequenceNr { get; set; }
        public event EventHandler<byte[]> NotificationReceived;

        public CsrmeshNetworkBase(int pin, string dst = null)
        {
            if (pin < 0 || pin > 9999)
                throw new ArgumentException("pin should be 4 digits only");
            Pin = pin;
            _dst = dst;
            SequenceNr = (int)DateTime.UtcNow.Subtract(new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds % 16777215;
            _log.LogInformation("Initialized seqNr to {0}", SequenceNr);
            NetworkKey = CalculateKey();
        }

         public async Task Connect(){
             await DoConnect();
             await DoSubscribeNotifications();
         }
         protected abstract Task DoConnect();
         protected abstract Task DoSubscribeNotifications();

        public int Pin { get; private set; }
        public byte[] NetworkKey { get; private set; }

        private void OnNotificationReceived(byte[] completeData){
            NotificationReceived?.Invoke(this,completeData);
        }

        protected void ProcessNotificationData(byte[] encryptedData){
            byte[] decryptedData = DecryptResponsePacket(encryptedData);
            _packetLog.LogDebug($"RCVenc: {BitConverter.ToString(encryptedData).Replace("-",":")} - RCVdec: {BitConverter.ToString(decryptedData).Replace("-",":")}");
            OnNotificationReceived(decryptedData);
        }

        public int GetNextSequenceNr()
        {
            if (++SequenceNr > MaxSequenceNr)
                SequenceNr = 1;
            return SequenceNr;
        }

        private byte[] CalculateKey()
        {
            string strPin = Pin.ToString("0000");
            byte[] bin =
                Encoding.UTF8.GetBytes(strPin)
                    .Concat(new byte[] { 0x00 })
                    .Concat(Encoding.UTF8.GetBytes("MCP"))
                    .ToArray();

            //generate key from 8 bin-bytes
            SHA256 sha256 = System.Security.Cryptography.SHA256.Create();
            byte[] hash = sha256.ComputeHash(bin);
            return hash.Reverse().Take(16).ToArray();
        }

        public byte[] DecryptResponsePacket(byte[] encryptedPacket)
        {
            if (encryptedPacket.Length < 8)
                throw new Exception("packet too short");

            int datalength = encryptedPacket.Length - 5;
            byte[] encryptedPayload = encryptedPacket.Skip(5).Take(datalength).ToArray();

 
            byte[] f = new byte[8]; //deviceNetworkKey aka 'NetworkIv'? not initialized

            Aes aes = Aes.Create();
            aes.Key = NetworkKey;
            aes.Mode = CipherMode.ECB;
            ICryptoTransform encryptor = aes.CreateEncryptor();
            byte[] @base = new byte[f.Length+8];//?? encryptedPacket.Take(5).Concat(new byte[10]).ToArray();
            Buffer.BlockCopy(encryptedPacket, 0, @base, 0, 3);
            Buffer.BlockCopy(encryptedPacket, 3, @base, 4, 2);
            Buffer.BlockCopy(f, 0, @base, 6, f.Length);
            byte[] ebase = encryptor.TransformFinalBlock(@base, 0, @base.Length).Take(datalength).ToArray();

            byte[] decryptedPayload = encryptedPayload.Zip(ebase, (b1, b2) => (byte)(b1 ^ b2)).ToArray();

            return encryptedPacket.Take(5).Concat(decryptedPayload).ToArray();
        }

        public byte[] DecryptPacket(byte[] encryptedPacket)
        {
            if (encryptedPacket.Length < 14)
                throw new Exception("packet too short");

            int datalength = encryptedPacket.Length - 14;
            uint seqNr = BitConverter.ToUInt32(encryptedPacket, 0);
            byte magic = encryptedPacket[4];
            byte[] encryptedPayload = encryptedPacket.Skip(5).Take(datalength).ToArray();
            byte[] hmac_packet = encryptedPacket.Skip(5 + datalength).Take(8).ToArray();
            byte eof = encryptedPacket.Skip(5 + datalength + 8).Single();

            byte[] prehmac = new byte[8].Concat(encryptedPacket.Take(5 + datalength)).ToArray();
            HMACSHA256 hmacsha256 = new HMACSHA256(NetworkKey);
            byte[] hmac_computed = hmacsha256.ComputeHash(prehmac).Reverse().Take(8).ToArray();

            Aes aes = Aes.Create();
            aes.Key = NetworkKey;
            aes.Mode = CipherMode.ECB;
            ICryptoTransform encryptor = aes.CreateEncryptor();
            byte[] @base = new byte[4 + 1 + 1 + 10];//?? encryptedPacket.Take(5).Concat(new byte[10]).ToArray();
            Buffer.BlockCopy(BitConverter.GetBytes(seqNr), 0, @base, 0, 4);
            @base[5] = magic;
            byte[] ebase = encryptor.TransformFinalBlock(@base, 0, @base.Length).Take(datalength).ToArray();

            byte[] decryptedPayload = encryptedPayload.Zip(ebase, (b1, b2) => (byte)(b1 ^ b2)).ToArray();

            return encryptedPacket.Take(5).Concat(decryptedPayload).ToArray();
        }

        public byte[] MakePacket(byte[] payloadData)
        {
            var nextSequenceNr = GetNextSequenceNr();
            _log.LogInformation("Sending data packet: 0x{0} seqNr: {1}", string.Concat(payloadData.Select(b => b.ToString("X2"))), nextSequenceNr);
            const byte magic = 0x80;
            const byte eof = 0xff;
            byte[] seqBytes = BitConverter.GetBytes(nextSequenceNr);
            int dataLength = payloadData.Length;

            var aes = Aes.Create();
            aes.Key = NetworkKey;
            aes.Mode = CipherMode.ECB;
            byte[] @base = new byte[16];
            Buffer.BlockCopy(seqBytes, 0, @base, 0, 4);
            @base[5] = magic;
            byte[] ebase = aes.CreateEncryptor().TransformFinalBlock(@base, 0, @base.Length).Take(dataLength).ToArray();

            byte[] payload = payloadData.Zip(ebase, (a, b) => (byte)(a ^ b)).ToArray();
            byte[] prehmac = new byte[8].Concat(seqBytes).Append(magic).Concat(payload).ToArray();
            byte[] hm = new HMACSHA256(NetworkKey).ComputeHash(prehmac).Reverse().Take(8).ToArray();
            byte[] result = seqBytes.Append(magic).Concat(payload).Concat(hm).Append(eof).ToArray();
            return result;
        }

        public class CsrPacket
        {
            public uint SequenceNr { get; set; }
            public byte Magic { get; set; }
            public byte[] EncryptedPayload { get; set; }
            public byte[] DecrypedPayload { get; set; }
            public byte[] HmacPacket { get; set; }
            public byte[] HmacComputed { get; set; }
            public byte Eof { get; set; }
        }

        protected abstract Task DoSendPacketAsync(byte[] packetData);
        
        public void SendPacket(byte[] payloadData, ushort baseHandle)
        {
            if (baseHandle != 0x21)
                throw new NotSupportedException();

            byte[] meshPacket = MakePacket(payloadData);
            _packetLog.LogDebug($"SNDenc: {BitConverter.ToString(meshPacket).Replace("-",":")} - SNDpay: {BitConverter.ToString(payloadData).Replace("-",":")}");

            DoSendPacketAsync(meshPacket).GetAwaiter().GetResult();
        }
    }
}