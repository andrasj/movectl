﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Move;
using movectl;

namespace movectlLib{
    class RequestResponseTracker {
        private readonly TaskCompletionSource<byte[]> result = new TaskCompletionSource<byte[]>();
        private readonly CsrmeshNetworkBase network;
        private readonly byte[] payload;
        private readonly ushort baseHandle;
        private readonly Predicate<byte[]> responseValidator;

        private static readonly ILogger logger=LogManager.Factory.CreateLogger("RequestTracker");

        public RequestResponseTracker(CsrmeshNetworkBase network,byte[] payload, ushort baseHandle, Predicate<byte[]> responseValidator)
        {
            this.network = network;
            this.payload = payload;
            this.baseHandle = baseHandle;
            this.responseValidator = responseValidator;
        }

        private void OnDataReceivedHandler(object sender,byte[] rcvdData)
        {
            logger.LogInformation("Received Data");
            if (!result.Task.IsCompleted && responseValidator(rcvdData))                        
                    result.TrySetResult(rcvdData);
        }

        internal async Task<byte[]> DoRequestAsync()
        {

            var cts = new CancellationTokenSource();
            try
            {
                logger.LogInformation("Subscribing for responses");
                network.NotificationReceived += OnDataReceivedHandler;
;
                cts.CancelAfter(TimeSpan.FromSeconds(5));
                network.SendPacket(payload, baseHandle);

                do{
                    var finishedTask = await Task.WhenAny(result.Task,Task.Delay(500,cts.Token));
                    if (finishedTask != result.Task)
                    {
                        network.SendPacket(payload, baseHandle);
                    }
                    else
                        break;
                }
                while (!cts.IsCancellationRequested);
                if (cts.IsCancellationRequested)
                    result.SetException(new TimeoutException());
                return await result.Task;
            }
            catch (OperationCanceledException)
            {
                result.SetException(new TimeoutException());
            }
            finally{
                logger.LogInformation("Unsubscribing for responses");
                network.NotificationReceived -= OnDataReceivedHandler;
            }
            return await result.Task;
        }

        internal byte[] DoRequest()
        {
            return DoRequestAsync().GetAwaiter().GetResult();
        }
    }
}