﻿using System;
using System.Linq;
using movectlLib;
using movectlLib.Commands;
using Microsoft.Extensions.Logging;
using Move;

namespace movectl
{
    public class MoveController
    {
        readonly ILogger _log = LogManager.Factory.CreateLogger("MoveController");

        private readonly CsrmeshNetworkBase _network;
        // private readonly RequestTracker _reqTracker;
        public const ushort BaseHandle = 0x0021;

        public MoveController(CsrmeshNetworkBase network)
        {
            _network = network;
            // _reqTracker = new RequestTracker(_network);
        }

        private TMoveResponse SendCommandWithResponse<TMoveResponse>(MoveCmd cmd) where TMoveResponse : MoveResp{
            var sw = System.Diagnostics.Stopwatch.StartNew();
            TMoveResponse expectedResponse = null;
            Predicate<byte[]> responseValidator = rcvdData=>{
                try{
                    var response = MoveResp.Parse(rcvdData);
                    var result = response is TMoveResponse && response.ObjectId == cmd.ObjectId;
                    if (result)
                        expectedResponse = (TMoveResponse) response;
                    return result;
                }
                catch (Exception e){
                    _log.LogWarning($"Failed validating response: {BitConverter.ToString(rcvdData).Replace("-",":")}: {e}");
                    return false;
                }
            };
            var ri = new RequestResponseTracker(_network,cmd.GetData(),BaseHandle, responseValidator);
            var responseBytes = ri.DoRequest();
            _log.LogInformation("received: "+expectedResponse +" in "+sw.ElapsedMilliseconds);
            if (expectedResponse == null)
                throw new Exception("No valid response received");
            return expectedResponse;
        }

        private void SendCommand(MoveCmd cmd)
        {
            byte[] data = cmd.GetData();
            _log.LogInformation("sent: "+BitConverter.ToString(data).Replace("-",":")+" - "+cmd.ToString());
            _network.SendPacket(data,BaseHandle);
  }

        public PositionSetResponse SetPosition(byte position, sbyte objid)
        {
            return SendCommandWithResponse<PositionSetResponse>(new GoToPosition(position, objid));
        }

        public PositionSetResponse SetPosition(byte position, byte speedUp, byte speedDown, sbyte objid)
        {
            return SendCommandWithResponse<PositionSetResponse>(new GoToPositionWithSpeed(position, speedUp, speedDown, objid));
        }

        public StatusResponse GetStatus(sbyte objid)
        {
            return SendCommandWithResponse<StatusResponse>(new GetStatus(objid));
        }
        
        public void WakeMesh()
        {
            SendCommand(new WakeMesh());
        }
    }
}
