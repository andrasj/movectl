﻿using System.Collections.Generic;

namespace movectl
{
    public class GoToPosition : MoveCmd
    {

        public GoToPosition(byte position, sbyte objId) : base(CmdId.GoToPosition, true, objId)
        {
            Position = position;
        }

        public byte Position { get; private set; }

        protected override IEnumerable<byte> GetPayload()
        {
            yield return Position;
        }

        public override string ToString()
        {
            return $"{base.ToString()} - Parameters - Position: {Position}";
        }

        public static MoveCmd ParseParameters(byte[] parameterData)
        {
            return new GoToPosition(parameterData[0], 0);
        }
    }
}