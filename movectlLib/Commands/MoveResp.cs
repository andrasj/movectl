using System.Diagnostics;
using System.IO;
using System.Linq;
//using System.Linq;

namespace movectl
{
    public abstract class MoveResp{
        public const byte Magic = 0x73;

        public RespId Id {get;}
        public byte ObjectId { get; private set; }
        public byte Flags { get; private set; }

        public bool IsSingleDevice => Flags == 0x80;
        public bool IsGroup => Flags == 0x00;

        public enum RespId{
            //MOVE_STATUS_OK
            Ok = 0x00,
            //MOVE_STATUS_INVALID_ID
            InvalidId = 0x01,
            //MOVE_STATUS_NO_MEM
            NoMem = 0x02,
            //MOVE_STATUS_NOT_TIMESYNCED
            NotTimesynched = 0x03,
            //MOVE_STATUS_REQ_INCOMPLETE
            RequestIncomplete = 0x04,
            //-106 MOVE_GET_STATUS_RSP
            StatusResponse = 0x96,
            //-94 MOVE_SET_POS_ACK
            PositionSetAck = 0xA2,
        }

        protected MoveResp(RespId id, sbyte objectId,byte flags){
            Id = id;
            ObjectId = (byte) objectId;
            Flags = flags;
        }


        public override string ToString()
        {
            return string.Format("Rsp: {0} Device: {1}", Id, IsSingleDevice ? ObjectId.ToString() : "Group-" + ObjectId);
        }

        public static MoveResp Parse(byte[] data)
        {
            byte srcObjectId = data[3];
            byte flags = data[4];
            data = data.Skip(5).ToArray(); //SeqNr & srcAddr
            MoveResp result;
            byte respId = data[3];
            switch (respId)
            {
                case (byte)RespId.StatusResponse:
                    result = StatusResponse.ParseParameters(data.Skip(4).ToArray());
                    break;
                case (byte)RespId.PositionSetAck:
                    result = PositionSetResponse.ParseParameters(data.Skip(4).ToArray());
                    break;
                default:
                    throw new InvalidDataException($"unexpected responseId: 0x{respId:x2}");
            }
            result.ObjectId = srcObjectId;
            result.Flags = flags;
            Debug.Assert(data[2] == Magic, "Expected the magic byte");
            return result;
        }

    }
}