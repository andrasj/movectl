﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
//using System.Linq;
using Move;

namespace movectl
{

    public abstract class MoveCmd
    {
        public CmdId Id { get; private set; }
        public byte ObjectId { get; private set; }
        public byte Flags { get; private set; }

        public bool IsSingleDevice => Flags == 0x80;
        public bool IsGroup => Flags == 0x00;

        public bool NeedsAck { get; }

        protected MoveCmd(CmdId id, bool needsAck, sbyte objectId)
        {
            Id = id;
            ObjectId = (byte)Math.Abs(objectId);
            Flags = (byte)(objectId <= 0 ? 0x00 : 0x80);
            NeedsAck = needsAck;
        }

        public enum CmdId
        {
            GoToPosition = 0x22,
            GetStatus = 0x16,
            SetDeactivationAction = 0x55,
            GetId = 0x10,
            WakeMesh = 0x45,
        }

        public const byte Magic = 0x73;

        public byte[] GetData()
        {
            return Enumerable.Empty<byte>()
                .Append(ObjectId)
                .Append(Flags)
                .Append(Magic)
                .Append((byte)Id)
                .Concat(GetPayload()).ToArray();
        }

        protected abstract IEnumerable<byte> GetPayload();

        public override string ToString()
        {
            return string.Format("Cmd: {0} Device: {1}", Id, IsSingleDevice ? ObjectId.ToString() : "Group-" + ObjectId);
        }

        public static MoveCmd Parse(byte[] data)
        {
            data = data.Skip(5).ToArray(); //SeqNr & 0x0080
            MoveCmd result;
            byte cmdId = data[3];
            switch (cmdId)
            {
                case (byte)CmdId.GoToPosition:
                    result = GoToPosition.ParseParameters(data.Skip(4).ToArray());
                    break;
                case (byte)CmdId.GetStatus:
                    result = GetStatus.ParseParameters(data.Skip(4).ToArray());
                    break;
                case (byte)CmdId.WakeMesh:
                    result = WakeMesh.ParseParameters(data.Skip(4).ToArray());
                    break;
                case (byte)CmdId.SetDeactivationAction:
                    result = DeactivationAction.ParseParameters(data.Skip(4).ToArray());
                    break;
                default:
                    throw new InvalidDataException($"unexpected commandId: 0x{cmdId:x2}");
            }
            result.ObjectId = data[0];
            result.Flags = data[1];
            Debug.Assert(data[2] == Magic, "Expected the magic byte");
            return result;
        }
    }
}