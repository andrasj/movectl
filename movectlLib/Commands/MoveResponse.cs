using System;

namespace movectl
{
    class MoveResponse
    {
        public MoveResponse(byte[] data)
        {
            Data = data;
        }

        public byte[] Data { get; }

        public override string ToString(){
            return BitConverter.ToString(Data).Replace("-",":");
        }
    }
}