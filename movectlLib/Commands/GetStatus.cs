using System.Collections.Generic;

namespace movectl
{
    public class GetStatus : MoveCmd
    {

        public GetStatus(sbyte objId) : base(CmdId.GetStatus, true,objId)
        {
        }

        protected override IEnumerable<byte> GetPayload()
        {
            yield break;
        }

        public override string ToString()
        {
            return $"{base.ToString()} - Parameters: None";
        }

        public static MoveCmd ParseParameters(byte[] parameterData)
        {
            return new GetStatus(0);
        }
    }
}