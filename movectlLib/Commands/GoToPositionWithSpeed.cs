﻿using System.Collections.Generic;

namespace movectl
{
    public class GoToPositionWithSpeed : MoveCmd
    {
        private static byte cmdId=0;

        public GoToPositionWithSpeed(byte position, byte speedUp, byte speedDown, sbyte objectId) : base(CmdId.GoToPosition, true, objectId)
        {
            Position = position;
            SpeedUp = speedUp;
            SpeedDown = speedDown;
        }

        public byte Position { get; private set; }
        public byte SpeedUp { get; private set; }
        public byte SpeedDown { get; private set; }

        protected override IEnumerable<byte> GetPayload()
        {
            yield return Position;
            yield return SpeedUp;
            yield return SpeedDown;
            yield return 0;
            yield return (byte)(((cmdId++ % 40) & 63) | 192);
        }

        public override string ToString()
        {
            return $"{base.ToString()} - Parameters - Position: {Position} - Speed: {SpeedUp}/{SpeedDown}";
        }

        public static MoveCmd ParseParameters(byte[] parameterData)
        {
            return new GoToPositionWithSpeed(parameterData[0], parameterData[1], parameterData[2], 0);
        }
    }
}