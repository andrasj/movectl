﻿using System.Collections.Generic;
using System.Linq;

namespace movectl
{
    public class DeactivationAction : MoveCmd
    {
        public byte AutomationId { get; private set; }
        public byte[] CmdAndParams { get; private set; }

        public DeactivationAction(byte automationId, byte[] cmdAndParams) : base(CmdId.GoToPosition, false,0)
        {
            AutomationId = automationId;
            CmdAndParams = cmdAndParams;
        }

        protected override IEnumerable<byte> GetPayload()
        {
            yield return AutomationId;
            foreach (var b in CmdAndParams)
                yield return b;
        }

        public override string ToString()
        {
            return $"{base.ToString()} - Parameters - AutomationId: {AutomationId} - CmdAndParams: nbBytes:{CmdAndParams.Length}";
        }

        public static MoveCmd ParseParameters(byte[] parameterData)
        {
            return new DeactivationAction(parameterData[0], parameterData.Skip(1).ToArray());
        }
    }
}