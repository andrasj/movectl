using System;
using System.Collections.Generic;
using System.Linq;

namespace movectl
{
    public class WakeMesh : MoveCmd
    {
        private static readonly byte[] _wakeFrame;
        static WakeMesh(){
            _wakeFrame = Enumerable.Empty<byte>()
            .Concat(new byte[]{100})
            .Concat(BitConverter.GetBytes((short)180))
            .Concat(new byte[]{4,255})
            .ToArray();

            if (_wakeFrame.Length != 5)
                throw new Exception("This is buggy, expected 5 bytes frameParameters");
        }

        public WakeMesh() : base(CmdId.WakeMesh,false,0)
        {
        }

        protected override IEnumerable<byte> GetPayload()
        {
            return _wakeFrame;
        }

        public override string ToString()
        {
            return $"{base.ToString()} - Parameters: None";
        }

        public static MoveCmd ParseParameters(byte[] parameterData)
        {
            return new WakeMesh();
        }
    }
}