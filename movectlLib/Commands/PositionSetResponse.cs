//using System.Linq;

namespace movectl
{
    public class PositionSetResponse : MoveResp{
        private PositionSetResponse(sbyte objectId, byte flags) : base(RespId.PositionSetAck, objectId, flags)
        {
        }

        public static MoveResp ParseParameters(byte[] parameterData)
        {
            return new PositionSetResponse(0,0);
        }

        public override string ToString()
        {
            return $"{base.ToString()} - Parameters: None";
        }
    }
}