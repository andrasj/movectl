//using System.Linq;

namespace movectl
{
    public class StatusResponse : MoveResp{
        private StatusResponse(sbyte objectId, byte flags,byte position,byte battery,byte sun,byte temperature) : base(RespId.StatusResponse, objectId, flags)
        {
            Position = position;
            Battery = battery;
            Sun = sun;
            Temperature = temperature;
        }

        public byte Position { get; }
        public byte Battery { get; }
        public byte Sun { get; }
        public byte Temperature { get; }

        public static MoveResp ParseParameters(byte[] parameterData)
        {
            return new StatusResponse(0,0,parameterData[0], parameterData[1], parameterData[2], parameterData[3]);
        }

        public override string ToString()
        {
            return $"{base.ToString()} - Param: Pos: {Position} Battery: {Battery} Sun: {Sun} Temperature: {Temperature}";
        }
    }
}