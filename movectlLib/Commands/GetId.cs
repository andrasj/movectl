﻿using System;
using System.Collections.Generic;
using System.Text;
using movectl;

namespace movectlLib.Commands
{
    class GetId : MoveCmd
    {
        public GetId(sbyte objectId) : base(CmdId.GetId, false, objectId)
        {
        }

        protected override IEnumerable<byte> GetPayload()
        {
            yield break;
        }
    }
}
