﻿// using System;
// using System.Collections.Generic;
// using System.Diagnostics;
// using System.IO;
// using System.Linq;
// using System.Threading;
// using System.Threading.Tasks;
// using Microsoft.Extensions.Logging;

// namespace Move
// {
//     public class CsrmeshNetwork : CsrmeshNetworkBase
//     {
//         private Process _process;
//         private readonly Timer _disconnectTimer;

//         private void AsyncDisconnect(object state)
//         {
//             Disconnect();
//         }

//         public CsrmeshNetwork(int pin, string dst = null) : base(pin, dst)
//         {
//             _disconnectTimer = new Timer(AsyncDisconnect, null, Timeout.Infinite, Timeout.Infinite);
//         }

//         public override void SendPacket(byte[] packetData, ushort handle)
//         {
//             lock (this)
//             {
//                 if (_process == null || _process.HasExited)
//                     Connect();

//                 var encryptedPacket = MakePacket(packetData);
//                 string bleHexData = string.Concat(encryptedPacket.Select(_ => _.ToString("x2")));
//                 var strHandle = handle.ToString("x4");
//                 var cmd = $"char-write-req {strHandle} {bleHexData}";
//                 _log.LogInformation("Writing: " + cmd);
//                 _process.StandardInput.WriteLine(cmd);
//                 Thread.Sleep(200);
//                 Expect(_process.StandardOutput, "Characteristic value was written successfully");
//                 Thread.Sleep(300);
//                 ScheduleDisconnect();
//             }
//         }

//         public void Disconnect()
//         {
//             lock (this)
//             {
//                 if (_process == null)
//                 {
//                     return;
//                 }
//                 if (_process.HasExited)
//                 {
//                     _process = null;
//                     return;
//                 }
//                 _log.LogInformation("Disconnecting");

//                 _process.StandardInput.WriteLine("disconnect");
//                 Thread.Sleep(200);
//                 //            Expect(process.StandardOutput, "successfully");

//                 _process.StandardInput.WriteLine("exit");
//                 Thread.Sleep(200);


//                 _process.WaitForExit();
//                 _log.LogInformation("Receiving: " + _process.StandardOutput.ReadToEnd());
//                 _log.LogInformation("ReceivingErr: " + _process.StandardError.ReadToEnd());
//                 var exitCode = _process.ExitCode;
//                 if (exitCode != 0)
//                 {
//                     _log.LogError("Unexpected ExitCode: " + exitCode);
//                     throw new Exception("Unexpected ExitCode: " + exitCode);
//                 }
//                 _log.LogInformation("Disconnected");
//             }
//         }

//         private void Connect()
//         {
//             lock (this)
//             {
//                 _log.LogInformation("Connecting");
//                 var call = string.Format("gatttool -b {0} -I", _dst);
//                 _process = CreateRemoteProcess(call);
//                 _process.Start();
//                 Thread.Sleep(200);
//                 _process.StandardInput.WriteLine("connect");
//                 Expect(_process.StandardOutput, "Connection successful");
//                 _log.LogInformation("Connected");
//                 ScheduleDisconnect();
//             }
//         }

//         private void ScheduleDisconnect()
//         {
//             _disconnectTimer.Change(TimeSpan.FromSeconds(5), Timeout.InfiniteTimeSpan);
//         }

//         private void Expect(StreamReader stream, string expect)
//         {
//             _log.LogInformation("reading untill: " + expect);
//             string readLine;
//             List<string> lines = new List<string>();
//             bool finished;
//             do
//             {
//                 Task<string> readTask = new Task<string>(stream.ReadLine);
//                 readTask.Start(TaskScheduler.Default);
//                 finished = readTask.Wait(3000);
//                 if (finished)
//                 {
//                     readLine = readTask.Result;
//                     _log.LogInformation(readLine);
//                 }
//                 else
//                     readLine = null;
//                 lines.Add(readLine);
//             } while (finished && readLine != null && readLine.IndexOf(expect, 0, StringComparison.OrdinalIgnoreCase) == -1);
//             if (readLine == null || readLine.Contains("Error"))
//                 throw new Exception("connection error, received: " + String.Join(Environment.NewLine, lines));
//         }

//         public void SendPacketSingleSession(byte[] packetData, ushort handle)
//         {
//             var encryptedPacket = MakePacket(packetData);
//             string bleHexData = string.Concat(encryptedPacket.Select(_ => _.ToString("x2")));
//             var call = string.Format("gatttool -b {0} --char-write-req -a 0x{1} -n {2}", _dst, handle.ToString("x4"), bleHexData);
//             var process = CreateRemoteProcess(call);
//             process.Start();
//             process.WaitForExit();
//             _log.LogInformation("Receiving: " + process.StandardOutput.ReadToEnd());
//             _log.LogInformation("ReceivingErr: " + process.StandardError.ReadToEnd());
//             var exitCode = process.ExitCode;
//             if (exitCode != 0)
//             {
//                 _log.LogError("Unexpected ExitCode: " + exitCode);
//                 throw new Exception("Unexpected ExitCode: " + exitCode);
//             }
//         }

//         private static Process CreateRemoteProcess(string call)
//         {
//             string args = @"zotac " + call;
//             args = @"root@192.168.10.20 ~/bluez-5.24/attrib/" + call;
//             var process = new Process();
//             if (File.Exists("/usr/bin/ssh"))
//                 process.StartInfo.FileName = @"/usr/bin/ssh";
//             else
//                 process.StartInfo.FileName = @"C:\Program Files\Git\usr\bin\ssh.exe";
//             process.StartInfo.Arguments = args;
//             process.StartInfo.UseShellExecute = false;
//             process.StartInfo.RedirectStandardError = true;
//             process.StartInfo.RedirectStandardInput = true;
//             process.StartInfo.RedirectStandardOutput = true;
//             process.StartInfo.CreateNoWindow = true;
//             return process;
//         }
//     }
// }