﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;
using Move;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace movectl.MqttApi
{
    public class MqttApi
    {
        private readonly MqttClient _mqttClient;
        private readonly string _mqttBrokerHost;
        private readonly string[] _incommingTopics;
        //private readonly string stateChangedTopic = "Move/State/{0}";

        private readonly CsrmeshNetwork _mesh;
        private readonly MoveController _controller;
        private static readonly ILogger<MqttApi> _log = Program.LogManager.CreateLogger<MqttApi>();


        public MqttApi()
        {
            _mqttBrokerHost = "192.168.10.3";
            _mqttClient = new MqttClient(_mqttBrokerHost);
            _mqttClient.MqttMsgPublishReceived += MqttClient_MqttMsgPublishReceived;
            _mqttClient.ConnectionClosed += _mqttClient_ConnectionClosed;
            _incommingTopics = new[] { "Move/CMD/#" };
            _mesh = new CsrmeshNetwork(1111, "43:C5:5B:04:00:06");
            _controller = new MoveController(_mesh);
        }

        private void _mqttClient_ConnectionClosed(object sender, EventArgs e)
        {
            _log.LogInformation($"Disconnected from Mqtt Broker: {_mqttBrokerHost}");
        }

        public void Start()
        {

            _log.LogInformation($"connecting to Mqtt Broker: {_mqttBrokerHost}");

            _mqttClient.Connect("MoveCtl");
            _mqttClient.Subscribe(_incommingTopics, new[] { (byte)0 });
            _log.LogInformation($"connecting to Mqtt Broker done");

            //List<SmartPlug> knownPlugs = new SmartPlugController().Get().Select(id => SmartPlugController.GetPlug(id)).ToList();
            //knownPlugs.ForEach(p =>
            //{
            //    p.SwitchedOnChanged += OnPlugStateChanged;
            //    p.StartMonitoring();
            //});

        }

        //private void OnPlugStateChanged(object sender, StateChangedEventArgs e)
        //{
        //    SmartPlug plug = (SmartPlug)sender;

        //    byte[] content = Encoding.UTF8.GetBytes($"{e.NewState.GetAsJson()}");
        //    _mqttClient.Publish(string.Format(stateChangedTopic, plug.Id), content);
        //}

        public void Stop()
        {
            //List<SmartPlug> knownPlugs = new SmartPlugController().Get().Select(id => SmartPlugController.GetPlug(id)).ToList();
            //knownPlugs.ForEach(p =>
            //{
            //    p.StopMonitoring();
            //    p.SwitchedOnChanged -= OnPlugStateChanged;
            //});

            _mqttClient.Unsubscribe(_incommingTopics);
            _mqttClient.Disconnect();
            _mesh.Disconnect();
        }

        private void MqttClient_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            try
            {
                _log.LogInformation($"received event");
                string topic = e.Topic;
                var match = Regex.Match(topic, @"Move/CMD/(?<name>\d+)/SetPosition");
                if (!match.Success)
                    return;

                int plugId = Convert.ToInt32(match.Groups["name"].Value);
                string body = Encoding.UTF8.GetString(e.Message, 0, e.Message.Length);

                int pos;
                if (int.TryParse(body, out pos))
                {
                    _log.LogInformation($"sending {pos} to plug {plugId}");
                    _controller.SetPosition((byte)pos, (sbyte)plugId);
                    _log.LogInformation($"sending {pos} to plug {plugId} done");
                }
                else
                {
                    _log.LogError("Invalid body received: " + body);
                }

            }
            catch (Exception ex)
            {
                _log.LogError($"Error processing message {e.Topic}", ex);
            }
        }

    }
}
