﻿using System;
using Microsoft.Extensions.Logging;

namespace movectl
{
    public class Program
    {
        public static ILoggerFactory LogManager = new LoggerFactory();
        //d609d30080e84d2654293783e60fa182d3f06d0e7cb388ff
        public static string p = "d6:09:d3:00:80:e8:4d:26:54:29:37:83:e6:0f:a1:82:d3:f0:6d:0e:7c:b3:88:ff";

        public static void Main(string[] args)
        {

            LogManager.AddConsole();

            // set Adapter
            var mqttApi = new MqttApi.MqttApi();
            mqttApi.Start();

            Console.ReadLine();

            mqttApi.Stop();

            return;
            //var controller = new MoveController(new CsrmeshNetwork(1111, "43:C5:5B:04:00:06"));
            //controller.SetPosition(0);
            //controller.SetPosition(50);
            //controller.SetPosition(255);

            //return;

            //int pin = 1111;
            //var csrmesh = new CsrmeshNetwork(pin);
            //var networkKey = csrmesh.NetworkKey;

            //Console.WriteLine(string.Concat(networkKey.Select(b => b.ToString("X").PadLeft(2, '0'))));

            //byte[] testPacket = p.Split(':').Select(strByte => Byte.Parse(strByte, NumberStyles.HexNumber)).ToArray();

            //CsrmeshNetwork.CsrPacket csrPacket = csrmesh.DecryptPacket(testPacket);

            //uint seq = csrPacket.SequenceNr;
            //var encDataPacket = csrmesh.MakePacket(csrPacket.DecrypedPayload );

            //Debug.Assert(encDataPacket.SequenceEqual(testPacket));

            //BruteForcePin(testPacket);
        }

        //public static void BruteForcePin(byte[] validPacket)
        //{
        //    //int i = 1111;
        //    for (int i = 0; i < 10000; i++)
        //    {
        //        var network = new CsrmeshNetwork(i);
        //        CsrmeshNetwork.CsrPacket testDecode = network.DecryptPacket(validPacket);
        //        if (testDecode.HmacComputed.SequenceEqual(testDecode.HmacPacket))
        //            Console.WriteLine("Found Pin: {0}", i);
        //    }
        //}




    }

}
